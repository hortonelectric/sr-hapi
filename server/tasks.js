'use strict';
const bitcore = require('bitcore');
const unirest = require('unirest');
const btcmath = require('./btcmath');
const _ = require("lodash");

// THIS SCRIPT IS EXPERIMENTAL! USE IT AT YOUR OWN RISK!
// seed hex:  4cc0af16fd589347b8867a2a1d47f4ce24b4647a0cce7f6cd7523abcda28ea335f69e1e39424bf338bcb9cf0e075d3b96e741a599a8f4f27f0184af3f8a46cfd
// main private key:  1859afd1c09daf1f0c8933675231e371c0aa7a46f39742e22938fbf7e20637b1
// main public key:  1F1bms3FK9RoN5NCbRAGKkM7k7JgYA6zyf
// hardened xprv hash m/0'/0:  xprv9ww5pk8jrgGkt5AFsrEuFk5p39bMfiuGGAWZPGxA6qj1Q1zX6V3ukyCGCqT6DsY6TotHnA3DLFMfy9eKXuKrkYxpWC2oR4wCy8Ba8VFhJxb
// hardened private key m/0'/0:  f793866f6f53098426dfe7296a2be3fff43c19e81f66c2c80416e6e2535e54d8
// hardened public key m/0'/0:  1BTVwpnY1gYgKQQh1akACjFXuWqRX19N3b
// hardened xpub hash m/0'/0 xpub6AvSEFfdh3q46ZEiysmuct2YbBRr5Bd7dPSABfMmfBFzGpKfe2NAJmWk45cRTwoFV4E7FPDjkk37pUAm4QkPdLNa8c91wa6Efb7NuvkUa3M
// child address of m/0'/0 with index 1 17iCnr5x1DtqmhLacKamAmQYkMpQiQGzXi
// child address of m/0'/0 with index 2 14xw2f25BnFryWmCbyNnYwGum1ZTM2WJ4D

const Async = require('async');
const SEEDHEX = "4cc0af16fd589347b8867a2a1d47f4ce24b4647a0cce7f6cd7523abcda28ea335f69e1e39424bf338bcb9cf0e075d3b96e741a599a8f4f27f0184af3f8a46cfd";
const XPUBKEY = "xpub6AvSEFfdh3q46ZEiysmuct2YbBRr5Bd7dPSABfMmfBFzGpKfe2NAJmWk45cRTwoFV4E7FPDjkk37pUAm4QkPdLNa8c91wa6Efb7NuvkUa3M";
const XWITHDRAWALSOURCE = "1F1bms3FK9RoN5NCbRAGKkM7k7JgYA6zyf";
const XHOUSE = "1ALPXbbmHec8F6h3gmFCo2vds6em3uoRzL";
const XWITHDRAWALSOURCE_PRIVKEY = "1859afd1c09daf1f0c8933675231e371c0aa7a46f39742e22938fbf7e20637b1";

const CHECK_BALANCE_LIMIT_MINUTES = 1;
const FEE = 20000;
const MINCASHOUT = 15460;

exports.register = function (server, options, next) {
    const Account = server.plugins['hapi-mongo-models'].Account;
	  const Purchase = server.plugins['hapi-mongo-models'].Purchase;


		const sendTx = function(fromAddress,amountToSend,toAddress,changeAddress,privateKeyString, callback) {
			var args = {
				fromAddress: fromAddress,
				amountToSend: amountToSend,
				toAddress: toAddress,
				changeAddress: changeAddress,
				privateKeyString: privateKeyString, 
				callback: callback
			};
			console.log("To:",toAddress,"Change:",changeAddress,"amount:",amountToSend);
			var options = "https://blockexplorer.com/api/addr/"+fromAddress+"/utxo";

			unirest.get(options)
			.send()
			.end(function (response) {
				var resbody = response.body;
				var utxos = [];
				var availableAmount = 0;

				for(var i=0;i<response.body.length;i++) {
					var thistx = response.body[i];
					var txout = {
					  "txId" : thistx.txid,
					  "vout" : thistx.vout,
					  "address" : thistx.address,
					  "scriptPubKey" : thistx.scriptPubKey,
					  "amount" : thistx.amount,
					  "fees": btcmath.toBTC(FEE)
					};
					if(true || thistx.confirmations && thistx.confirmations > 0) {
						availableAmount += btcmath.fromBTC(thistx.amount);
						utxos.push(new bitcore.Transaction.UnspentOutput(txout));
					}
				}

				if(availableAmount <  args.amountToSend) {
					return callback( {err:"insufficient funds"});//self.view("app", {secret:secret,err: "insufficient funds"});
				}

				try {
					var toAddress = new bitcore.Address(args.toAddress);
					var privateKey = new bitcore.PrivateKey(args.privateKeyString);
					var changeAddress = new bitcore.Address(args.changeAddress);


					var transaction = new bitcore.Transaction()
				    .from(utxos)          // Feed information about what unspent outputs one can use
				    .to(toAddress, args.amountToSend)  // Add an output with the given amount of satoshis
				    .change(args.changeAddress)      // Sets up a change address where the rest of the funds will go
				    .sign(privateKey);     // Signs all the inputs it can
					var hash = transaction.toObject().hash;
					var serialized = transaction.serialize();
					console.log(serialized);
					unirest.post("http://btc.blockr.io/api/v1/tx/push")
					.send({hex:serialized})
					.end(function (response) {
						// console.log(response.body,"btcblockr");
						unirest.post("https://api.coinprism.com/v1/sendrawtransaction")
						.send(serialized)
						.end(function (response) {	
							// console.log(response.body);	
							unirest.post("https://blockchain.info/pushtx")
							.send({tx:serialized})
							.end(function (response) {	
								// console.log(response.body);	
										
							});						
						});
					});
					return callback(transaction.toObject());
				} catch(e) {
					console.log(e);
					return callback({err: e});
				}

			});		
		};

		const checkTx = function(addresses,purchases, callback) {
			var options = "https://blockexplorer.com/api/addrs/"+addresses+"/txs";
			console.log("all txs",options);
			unirest.get(options)
			.send()
			.timeout(10000)
			.end(function (response) {
				if(!response.body || !response.body.items) {
					console.log("nothing in response");
					return callback("nothing in response");
				}
				// console.log(response.body);
				for(var i=0;i<response.body.items.length;i++) {
					var txresponse = response.body.items[i];
					console.log("incoming payment tx", txresponse.txid);

					if(txresponse.isCoinBase && txresponse.confirmations < 100) {
						// do nothing
						continue;
					} else if(txresponse.confirmations > 0) {

						var totalValue = 0;

						var purchase = undefined;
						for(var x=0;x<txresponse.vout.length;x++) {
							if(txresponse.vout[x].scriptPubKey.addresses.length > 1) {
								return {err:"Error code 1. Please report this error code to the support staff."};
							}	
							var thisAddr = txresponse.vout[x].scriptPubKey.addresses[0];
							purchase = _.find(purchases,{bitcoinAddressApp:thisAddr});
							if(purchase && thisAddr === purchase.bitcoinAddressApp) {
								purchase.bitcoinReceiveTx = txresponse.txid;
								totalValue += btcmath.fromBTC(txresponse.vout[x].value);
								break;
							}						
						}
						if(!purchase) {
							return callback("This transaction has nothing to do with the game.");
						}
						if(totalValue >= purchase.satoshiPrice) {

		          Async.auto({
		          	sellerAccount: function(done) {
		          		Account.findById(purchase.sellerId, function(err,sellerAccount) {
		          			if(err || !sellerAccount) {
		          				return done("FAILED AT SELLER ACCOUNT LOOKUP");
		          			}
		          			// console.log(sellerAccount.user.name,sellerAccount.inventory);
		          			var soldItem;
		          			for(var inv=0;inv<sellerAccount.inventory.length;inv++) {
											if(sellerAccount.inventory[inv].listDate && sellerAccount.inventory[inv].claimDate && purchase.item.data.code === sellerAccount.inventory[inv].data.code) {
												soldItem = sellerAccount.inventory[inv];
												sellerAccount.inventory.splice(inv,1);
											}
		          			}
		          			if(!soldItem) {
		          				return done("FAILED AT SELLER ITEM LOOKUP");

		          			}
		          			sellerAccount.itemsSold.push(purchase);
		          			return done(null, sellerAccount);
		          		});
		          	}, sendTx: ['sellerAccount', function(done,results) {
	          			if(!results.sellerAccount) {
	          				return done("NO SELLER ACCOUNT FOUND");
	          			}
	          			var sellerAccount = results.sellerAccount;
									var hdPrivateKey = new bitcore.HDPrivateKey.fromSeed(SEEDHEX);
									var hard = hdPrivateKey.derive("m/0'/0");
									var xpubPRIVKEY = hard.derive("m/"+purchase.bitcoinAddressAppCount, bitcore.Networks.livenet);						

									sendTx(purchase.bitcoinAddressApp,purchase.satoshiSeller,purchase.bitcoinAddressSeller,XWITHDRAWALSOURCE,xpubPRIVKEY.privateKey.toString(), function(transaction) {
										if(transaction.err) {
											console.log(transaction.err);
											return done(transaction.err);
										}
										console.log("sent",transaction.hash);
										purchase.bitcoinSendTx = transaction.hash;
										purchase.timeConfirmed = new Date();

										Purchase.findByIdValidateAndUpdate(purchase._id, purchase, (err, purchase) => {

						          if (err) {
						              return done(err);
						          }		          			
											Account.findByIdValidateAndUpdate(sellerAccount._id,sellerAccount,sellerAccount, function(err) {
					          		if(err) {
					          			return done("FAILED AT SELLER ACCOUNT UPDATE");
					          		}				          			
												return done(null, transaction.hash);
			          			});		
			          		});
			          	});
		          	}],
		          	buyer: ['sendTx', function(done,results) {
		          		if(!results.sendTx) {
		          			return done("Transaction Failed");
		          		}
				          Account.findOne({"purchases.bitcoinAddressApp": purchase.bitcoinAddressApp}, function(err,account) {
				          	if(!account || err) {
				          		console.log(err);
				          		return done("FAILED AT LOOKUP BUYER ACCOUNT");
				          	}
				          	var unconfirmedStill = false;
				          	for(var p=0;p<account.purchases.length;p++) {
				          		if(account.purchases[p].bitcoinAddressApp === purchase.bitcoinAddressApp) {
				          			account.purchases[p] = purchase;
				          			
						          	account.inventory.push(account.purchases[p].item);
				          		} else if(!account.purchases[p].timeConfirmed) {
				          			unconfirmedStill = true;
				          		}
				          	}
				          	if(!unconfirmedStill) {
				          		account.unconfirmedPurchases = false;
				          	}
				          	Account.findByIdValidateAndUpdate(account._id,account,account, function(err) {
				          		if(err) {
				          			return done("FAILED AT BUYER ACCOUNT UPDATE");
				          		}
				          		return done();
				          	});
				          });
		          	}]
		          }, (err, results) => {

                if (err) {
	                	console.log(err);
	                	return false;
                }

                return true;
		          });	
						}
					}										
				};
				return callback();								
			},function(error) {
				console.log("ERROR BLOCKCHAIN");		
				return callback("BLOCKCHAIN FETCH ERROR");				
			});
		};
    server.method("checkTransactions",function (request, reply) {
    	// console.log("CHECKING TXs");
    	Async.auto({
    		clearOld: function(callback) {
    			// console.log('clearing 5 days old transactions');

    			// var now = new Date().getTime();
    			// var fiveDaysOld = new Date(now-60);
    			var now = new Date().getTime();
    			var fiveDaysOld = new Date(now-86400000/2); //fuck that, 12 hours is enough
    			const updateWhich = {timeConfirmed:{$exists:false}, timeCreated: {$lt:fiveDaysOld}};
					Purchase.updateMany(updateWhich, {$set:{canceled:true}}, function(err) {
					
						if(err) {
							return callback(err);
						}			
						Account.updateMany(
							{"purchases.timeCreated":{$lt:fiveDaysOld}, "purchases.timeConfirmed":{$exists:false}}, 
							{$pull:{purchases:{timeCreated:{$lt:fiveDaysOld}}}}, 
							function(err) {
								if(err) {
									return callback(err);
								}

								Account.updateMany(
									{"inventory.claimDate":{$lt:fiveDaysOld}}, 
									{$unset:{'inventory.$.claimDate':true}}, 
									function(err) {
										if(err) {
											return callback(err);
										}
										return callback();
								});						
						});
					});
    		},
    			checkTxs: function(callback) {
    				// console.log('looking for new purchases');
						Purchase.find({timeConfirmed:{$exists:false},canceled:{$exists:false}}, function(err,purchases) {
							var addresses = [];
							for(var i=0;i<purchases.length;i++) {
								addresses.push(purchases[i].bitcoinAddressApp);
							}
							if(!addresses.length) {
								return callback("Nothing to do");								
							}
							checkTx(addresses.join(','), purchases, function(err) {
								if(err) { 
									return callback(err);
								}
								return callback();
							});
						});    				
    			}
    		
    	}, (err, results) => {

	      if (err) {
	          return reply(err);
	      }

	      return reply(null);
	    }); 
    });

    next();
};


exports.register.attributes = {
    name: 'tasks'
};
