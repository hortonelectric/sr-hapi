'use strict';
const XPUBKEY = "xpub6AvSEFfdh3q46ZEiysmuct2YbBRr5Bd7dPSABfMmfBFzGpKfe2NAJmWk45cRTwoFV4E7FPDjkk37pUAm4QkPdLNa8c91wa6Efb7NuvkUa3M";
const FEE = 20000;
const Boom = require('boom');
const Async = require('async');
const Joi = require('joi');

var crypto = require('crypto');
var bitcore = require('bitcore');
var yaqrcode = require('yaqrcode');
var btcmath = require('../btcmath');
var provable = require('../provably-fair');
var bignumber = require('bignumber.js');

const AuthPlugin = require('../auth');
// Load Chance
var Chance = require('chance');
var _ = require('lodash');

// Instantiate Chance so it can be used
var chance = new Chance();
const internals = {};

internals.applyRoutes = function (server, next) {

    const Pusher = server.plugins['hapi-mongo-models'].Pusher;
    const SourceDetail = server.plugins['hapi-mongo-models'].SourceDetail;
    const User = server.plugins['hapi-mongo-models'].User;
    const Status = server.plugins['hapi-mongo-models'].Status;
    const Account = server.plugins['hapi-mongo-models'].Account;
    const Purchase = server.plugins['hapi-mongo-models'].Purchase;

    const gameConfig = server.settings.app.gameConfig;

    const hasItem = function(inventory,itemCode) {
      for(var i=0;i<inventory.length;i++) {
        if(inventory[i].data.code === itemCode && !inventory[i].listDate) {
          return true;
        }
      }
      return false;
    };

    const gameTime = function(request,reply) {
      if(request.pre.skip) { return reply(false); }
      const id = request.auth.credentials.roles.account._id.toString();
      const update = {};
      update.$inc = {
        gametime: 1
      };

      if(hasItem(request.pre.account.inventory,"39")) {
        update.$inc.cash = request.pre.account.level*10000;
      }else if(hasItem(request.pre.account.inventory,"23")) {
        update.$inc.cash = request.pre.account.level*100;
      } else if(request.pre.account.level > 0 && request.pre.account.heat > 1) {
        update.$inc.cash = request.pre.account.level*1;
      }

      Account.findByIdValidateAndUpdate(id, request.pre.account, update, (err, account) => {

          if (err) {
              return reply(err);
          }

          reply(account);
      });
    };
    const skipSourceCheck = function(request,reply) {
        var account = request.pre.account;
        if(account.sourceNumber === parseInt(request.params.sourceNumber)) {
          return reply(true);
        }
        reply(false);
      };
    const getAccount = function(request,reply) {
      const id = request.auth.credentials.roles.account._id.toString();

      Account.findById(id, (err, account) => {

          if (err) {
              return reply(err);
          }


          var update = {$set: {}};
          if (!account) {
              return reply(Boom.notFound('Document not found. That is strange.'));
          }
          // hold correct?
          if(account.hold !== gameConfig.defaultHold+(account.level*5)) {
            update.$set.hold = gameConfig.defaultHold+(account.level*5);
          }
      

          var nextLevel = 0,nextLevelCost = 0;
          for(var i=0;i<gameConfig.levels.length;i++) {
            if(account.xp >= gameConfig.levels[i].xp && account.cash >= gameConfig.levels[i].cash) {
              nextLevel = gameConfig.levels[i].level;
              nextLevelCost = gameConfig.levels[i].cash;
            }
          }
          if(account.nextLevel !== nextLevel) {
            update.$set.nextLevel = nextLevel;
            update.$set.nextLevelCost = nextLevelCost;
          }

          if(Object.keys(update.$set).length) {

            Account.findByIdValidateAndUpdate(id, account, update, (err, account) => {

                if (err) {
                    return reply(err);
                }

                return reply(account);
            });
          } else {
            return reply(account);
          }
      });
    };
    const encounter = function(request,reply) {
        if(request.pre.skip) { return reply(false); }
        const id = request.auth.credentials.roles.account._id.toString();

        var randEnc = chance.integer({min:0,max:100});
        var opponentTypes = ["Vigilantes", "Undercover Police", "Government Agents"];
        var opponentType = opponentTypes[chance.integer({min:0,max:2})];
        var maxOpponents = 2+(request.pre.account.level);
        if(maxOpponents > 10) { maxOpponents = 10; }
        var numOpponents = chance.integer({min:1*request.pre.account.level,max:maxOpponents});
        // var numOpponents = 6;
        var opponents = [];
        var encounterStatus = "";
        if(randEnc < request.pre.account.heat) {
        // if(true) {
          while(numOpponents > opponents.length) {
            const wc = server.settings.app.weaponsConfig.weaponsByLevel;
            var weaponLevel = request.pre.account.level+2;
            if(weaponLevel > 10) { weaponLevel = 10; }
            // var weaponLevel = chance.integer({min:0,max:weaponLevelMax});
            var weaponIndex = chance.integer({min:0,max:wc[weaponLevel].length-1});
            opponents.push({
              weapon: wc[weaponLevel][weaponIndex],
              type: opponentType,
              hp: chance.integer({min:8,max:server.settings.app.gameConfig.maxhp}),
              armor: chance.integer({min:6,max:10+request.pre.account.level})
            });
          }
          encounterStatus = opponentType + " are approaching!";
        }
        var update = {
          $set: {
            encounterStatus: encounterStatus,
            opponents: opponents
          }
        }
        Account.findByIdValidateAndUpdate(id, request.pre.account, update, (err, account) => {

            if (err) {
                return reply(err);
            }
            reply(opponents);
        });
    };
    const prices = function(request, reply) {
      if(request.pre.skip) { return reply(false); }
      const id = request.auth.credentials.roles.account._id.toString();
      var source = {};
      for(var i=0;i<request.pre.account.sourcesList.length;i++) {
        if(request.pre.account.sourcesList[i].sourceNumber === parseInt(request.params.sourceNumber)) {
          source = request.pre.account.sourcesList[i];
        }
      }
      if (!source) {
          return reply(Boom.notFound('Source not found. That is strange.'));
      }

      const cashTierRange = chance.weighted([2, 3, 5, 7, 9], [2,1.75,1.5,1.25,1]);
      const randomCash = Math.ceil(chance.integer({min:2000*cashTierRange,max:30000*cashTierRange})*(request.pre.account.level+1));


      const goods = [];
      var defaultPricesArray = [];
      var defaultGoodsArray = [];
      for(var t in gameConfig.goodsPrices) {
        defaultGoodsArray.unshift( {type:t,price:gameConfig.goodsPrices[t]} );
      }
      defaultGoodsArray.sort(function(a,b) {
        return a.price < b.price; // low to high prices
      });
      defaultPricesArray = _.chain(defaultGoodsArray).map('price').flattenDeep().value();
      defaultPricesArray.sort(function(a,b) {
        return a.price > b.price; // high to low prices
      });
      for(var i in gameConfig.goodsPrices) {
        const tierRange = chance.weighted([1.25, 1.75, 2.25, 2.75, 3.25], [2,1.75,1.5,1.25,1]);

        var priceLevelFactor = 1;
        for(var x=0;x<request.pre.account.level;x++) {
          priceLevelFactor *= server.settings.app.gameConfig.priceLevelFactor;
        }
        var pricePrefferedFactor = 1;
        if(source.prefBuy === i) {
          pricePrefferedFactor = 1.25; // user prefers to buy this item at a favorably high price
        } else if(source.prefSell === i) {
          pricePrefferedFactor = 0.75;    // user prefers to sell this item at a favorably low price      
        }
        var rando = chance.floating({fixed:2,min:1,max:tierRange})*priceLevelFactor*pricePrefferedFactor;
        const randomPrice = Math.ceil(gameConfig.goodsPrices[i] * rando);

        const maxRandomAmount = chance.weighted([10,20,30,40,50,60], defaultPricesArray);
        var maxRandomMultiplier = 1;
        for(var gi=0;gi<defaultGoodsArray.length;gi++) {
          if(i === defaultGoodsArray[gi].type) {
            maxRandomMultiplier = gi+1;
            break;
          }
        }
        const randomAmount = Math.ceil(chance.integer({min:1,max:maxRandomAmount})*(request.pre.account.level+1)*maxRandomMultiplier);
        goods.push({price: randomPrice, amount: randomAmount, type: i});
      }
      var hp = request.pre.account.hp;
      hp++;
      if(hasItem(request.pre.account.inventory,"27")) {
        hp++;
      }
      if(hasItem(request.pre.account.inventory,"42")) {
        hp+=5;
      }      
      if(hp > request.pre.account.maxhp) {
        hp = request.pre.account.maxhp;
      }
      const update = {};
      update.$set = {
        sourceCash: randomCash,
        sourceGoods: goods,
        currentSource: source,
        sourceNumber: source.sourceNumber,
        hp: hp
      };


      Account.findByIdValidateAndUpdate(id, request.pre.account, update, (err, account) => {

          if (err) {
              return reply(err);
          }
          reply(true);
      });

    };


    server.route({
        method: 'GET',
        path: '/qrcode/{data}',
        config: {},
          handler: function(request,reply) {
            const data = yaqrcode(request.params.data,{size:250});
            
            function decodeBase64Image(dataString) {
              var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
                response = {};

              if (matches.length !== 3) {
                return new Error('Invalid input string');
              }

              response.type = matches[1];
              response.data = new Buffer(matches[2], 'base64');

              return response;
            }

            var imageBuffer = decodeBase64Image(data);     
            return reply(imageBuffer.data).type('image/gif');
            ;
          }
        });
    
    server.route({
        method: 'GET',
        path: '/account',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'account'
            },
            pre :[{
                method: getAccount,
                assign: 'account'
            }]
          },
          handler: function(request,reply) {
            request.pre.account.config = gameConfig;
            return reply(request.pre.account);
          }
        });
    

    const levelUpGift = function(request,reply) {
      const gifts = _.concat(server.settings.app.gearConfig.byLevel[request.pre.account.nextLevel],server.settings.app.weaponsConfig.weaponsByLevel[request.pre.account.nextLevel])
      return reply(gifts);
    };
    server.route({
        method: 'GET',
        path: '/levelup',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'account'
            },
            pre :[{
                method: getAccount,
                assign: 'account'
            },{
                method: levelUpGift,
                assign: 'giftsList'
            }]
          },
          handler: function(request,reply) {
            request.pre.account.config = gameConfig;
            request.pre.account.giftsList = request.pre.giftsList;
            return reply(request.pre.account);
          }
        });

    server.route({
        method: 'POST',
        path: '/levelup',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'account'
            },
            pre :[{
                method: getAccount,
                assign: 'account'
            },{
                method: levelUpGift,
                assign: 'giftsList'
            }]
          },
          handler: function(request,reply) {
            const id = request.auth.credentials.roles.account._id.toString();
            var nextLevel = gameConfig.levels[request.pre.account.nextLevel];
            var newCash = request.pre.account.cash;
            newCash -= nextLevel.cash;


            if(newCash < 0) {
              return reply(Boom.badRequest("Not enough cash"));
            }
            if(request.pre.account.xp < nextLevel.xp) {
              return reply(Boom.badRequest("Not enough XP"));
            }
            if(request.pre.account.level >= nextLevel.level) {
              return reply(Boom.badRequest("Can't level up to a previous level."));
            }
            const newGift = request.pre.giftsList[request.payload.giftIndex];
            if(!newGift) {
              return reply(Boom.badRequest("Sorry, that gift isn't available"));
            }

            if(newGift.type ==='weapon') {
              var newInventory = {
                type: "weapon",
                data: request.pre.giftsList[request.payload.giftIndex]
              };
              request.pre.account.inventory.push(newInventory);

            } else if(newGift.type ==='gear') {
              var newInventory = {
                type: "gear",
                data: request.pre.giftsList[request.payload.giftIndex]
              };
              request.pre.account.inventory.push(newInventory);
            }

            // new mate with default weapon
            const wc = server.settings.app.weaponsConfig.defaultWeapon;
            request.pre.account.teammates.push({hp:30,weapon:wc,type:chance.first(),armor:12+request.pre.account.nextLevel});

            // max HP correct?
            request.pre.account.maxhp = gameConfig.defaultHp+(request.pre.account.nextLevel*3);

            request.pre.account.cash = newCash,
            request.pre.account.heat = 0,
            request.pre.account.level = request.pre.account.nextLevel,
            request.pre.account.hp = request.pre.account.maxhp,
            request.pre.account.nextLevel = -1,
            request.pre.account.nextLevelCost = -1,
            request.pre.account.levelPowerupHeat = false,
            request.pre.account.levelPowerupHp = false;              


              Account.findByIdValidateAndUpdate(id, request.pre.account, request.pre.account, (err, account) => {

                if (err) {
                    return reply(err);
                }
                account.config = gameConfig;
                reply(account);
            });
          }
        });

    server.route({
        method: 'GET',
        path: '/game-source/{sourceNumber}',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'account'
            },
            pre :[{
                method: getAccount,
                assign: 'account'
            }, {
                method: skipSourceCheck,
                assign: 'skip'
            }, {
                method: gameTime,
                assign: 'account'
            }, {
                assign: 'opponents',
                method: encounter
            }, {
                method: prices
            }]
        },
        handler: function (request, reply) {

          const id = request.auth.credentials.roles.account._id.toString();

          Account.findById(id, (err, account) => {

              if (err) {
                  return reply(err);
              }

              if (!account) {
                  return reply(Boom.notFound('Document not found. That is strange.'));
              }
              account.config = gameConfig;
              reply(account);
          });
        }
    });
    server.route({
      method:"GET",
      path: "/powerups/{action}",
      config: {
        auth: {
          strategy: 'simple',
          scope: 'account'
        }
      },
      handler: function(request,reply) {
        const id = request.auth.credentials.roles.account._id.toString();
        Account.findById(id, (err, account) => {
          const update = {};

          if (!account) {
              return reply(Boom.notFound('Document not found. That is strange.'));
          }
          const getPowerupCost = function() {
            return gameConfig.powerupCost*Math.pow(gameConfig.powerupFactor,account.level);
          };
          var minCash = 0;
          switch(request.params.action) {
            case "laylow":
              account.heat -= 10;
              account.cash -= getPowerupCost();
              if(account.levelPowerupHeat === true && !hasItem(account.inventory,"38")) {
                return reply(Boom.badRequest("You can only use this power up once per level."));
              }
              account.levelPowerupHeat = true;
              break;
            case "heal":
              account.cash -= getPowerupCost();
              account.hp += 10;

              if(account.levelPowerupHp === true && !hasItem(account.inventory,"28")) {
                return reply(Boom.badRequest("You can only use this power up once per level."));
              }

              account.levelPowerupHp = true;
              break;
            default:
              return reply(Boom.badRequest('You did not ask for a powerup.'));              
          }
          if (account.cash < 0) {
              return reply(Boom.badRequest('Not enough money!'));
          }
          if(account.heat < 0) {
            account.heat = 0;
          }
          if(account.hp > account.maxhp) {
            account.hp = account.maxhp;
          }
          Account.findByIdValidateAndUpdate(id, account, account, (err, account) => {
              if (err) {
                  return reply(err);
              }
              account.config = gameConfig;
              reply(account);
          });
        });
      }
    });

    server.route({
        method: 'POST',
        path: '/transaction',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'account'
            },
            pre: [{
                method: getAccount,
                assign: 'account'
            }]
        },
        handler: function (request, reply) {
          const id = request.auth.credentials.roles.account._id.toString();
          Account.findById(id, (err, account) => {

              if (err) {
                  return reply(err);
              }

              if (!account) {
                  return reply(Boom.notFound('Document not found. That is strange.'));
              }
              var thisSourceGoods = {};
              var thisMyGoods = {};
              const goodsAmount = parseInt(request.payload.goodsAmount,10);
              if(!goodsAmount) {
                return reply(Boom.notFound("You asked for 0. Check your stats."));
              }
              for(var i=0;i<account.sourceGoods.length;i++) {
                if(account.sourceGoods[i].type === request.payload.goodsType) {
                  thisSourceGoods = account.sourceGoods[i];
                  break;
                }
              }
              if(!Object.keys(thisSourceGoods).length) {
                return reply(Boom.notFound("those goods were not found."));
              }
              var newCash = account.cash || 0;
              const salePrice = Math.ceil(thisSourceGoods.price * goodsAmount);

              if(request.payload.txType === 'buy') {
                newCash = newCash - salePrice;
                if(thisSourceGoods.amount-goodsAmount < 0) {
                  return reply(Boom.notFound("Not enough goods available to buy "+goodsAmount));
                }
                if(newCash < 0) {
                    return reply(Boom.notFound("You cannot afford those goods"));
                }
                var totalGoods = 0;
                thisSourceGoods.amount -= goodsAmount;

                account.sourceCash += salePrice;

                for(var x=0;x<account.goods.length;x++) {
                  totalGoods += account.goods[x].amount;
                  if(account.goods[x].type === request.payload.goodsType) {
                    account.goods[x].amount += goodsAmount;
                    account.goods[x].price = thisSourceGoods.price;
                    thisMyGoods = account.goods[x];
                  }
                }
                if(account.hold - totalGoods < goodsAmount && !hasItem(account.inventory,"34") ) {
                  return reply(Boom.notFound("You cannot hold that many"));
                }
                if(!Object.keys(thisMyGoods).length) {
                  account.goods.push({amount: goodsAmount, price: parseInt(thisSourceGoods.price,10), type: thisSourceGoods.type})
                }
              } else if (request.payload.txType === 'sell') {
                if(account.sourceCash < salePrice) {
                  return reply(Boom.notFound("Source cannot afford those goods"));
                }
                newCash = newCash + salePrice;
                account.sourceCash -= salePrice;
                thisSourceGoods.amount += goodsAmount;

                for(var x=0;x<account.goods.length;x++) {
                  if(account.goods[x].type === request.payload.goodsType) {
                    account.goods[x].amount -= goodsAmount;
                    if(account.goods[x].amount < 0) {
                      return reply(Boom.notFound("Not enough goods to sell"));
                    } else if (account.goods[x].amount === 0) {
                      account.goods.splice(x,1);
                    }
                    break;
                  }
                }
              }
              const update = {};

              update.$set = {
                cash: newCash,
                goods: account.goods,
                sourceGoods: account.sourceGoods,
                sourceCash: account.sourceCash
              };
              var xpInc = hasItem(account.inventory,"25")?2:1;
              update.$inc = {
                heat: 1,
                xp: xpInc
              };
              Account.findByIdValidateAndUpdate(id, account, update, (err, account) => {

                  if (err) {
                      return reply(err);
                  }
                  account.config = gameConfig;
                  reply(account);
              });
          });
        }
    });

    const encounterResponse = function(request, reply) {
      const id = request.auth.credentials.roles.account._id.toString();

      const calcHit = function(w,target) {
        if(!w) { return false; }
        var critical = "";
        var superCritical = "";
        var roll = chance.d20()+w.hitModifier;
        var damage = 0;
        if(roll >= target.armor) {
          var damage = w.damageModifier;
          switch(w.damageClass) {
            case 4:
              damage += chance.d4();
              break;
            case 6:
              damage += chance.d6();
              break;
            case 8:
              damage += chance.d8();
              break;
            case 10:
              damage += chance.d10();
              break;
            case 12:
              damage += chance.d12();
              break;
          }

          if(roll === 20) {
            superCritical = " MASSIVE ";
            damage += damage;
          }
        }
        return {critical:critical,superCritical:superCritical,damage:damage};
      };
      const attack = function(w,t,attackerString,targetString) {
        
        var args = {
          w: arguments['0'],
          t: arguments['1'],
          attackerString: arguments['2'],
          targetString: arguments['3']
        }
        // overwrite user object to keep extra data from clogging up the system
        if(args.t._id === request.pre.account._id) {
          var armorBonus = 0;
          for(var i=0;i<request.pre.account.inventory;i++) {
            if(request.pre.account.inventory[i].data.code === "43") {
              armorBonus += 2;
            } else if(request.pre.account.inventory[i].data.code === "44") {
              armorBonus += 4;
            } else if(request.pre.account.inventory[i].data.code === "45") {
              armorBonus += 6;
            }
          }
          args.t = {
            player: true,
            hp: args.t.hp,
            weapon: args.t.weapon,
            armor: args.t.armor+armorBonus
          };
        } 
        var result = calcHit(w, args.t);
        _.extend(args,result);
        if(result.damage > 0) {
          encounterEvents.push({args:args, string:attackerString+" attacked " + targetString + " with a " + w.name + " and caused "+result.superCritical+result.critical+result.damage+" damage! " });
        } else {
          encounterEvents.push({args: args, string: attackerString+" attacked " + targetString + " with a "+w.name+" but missed!" });
        }
        return result.damage;
      };
      var encounterEvents = [];
      var isFight = true;

      if(request.payload.action === "bribe") {
        var minBribeCash = (request.pre.account.level+1)*2000;
        for(var l=1;l<request.pre.account.level;l++) {
          minBribeCash *= 10;
        }
        var bribeChance = hasItem(request.pre.account.inventory,"26")?20:0;

        if(chance.integer({min:1,max:100}) > 30-bribeChance && request.pre.account.cash >= gameConfig.minBribeCash) {
          isFight = false;
          request.pre.account.opponents = [];
          var bribeAmount = hasItem(request.pre.account.inventory,"31")?4:2;
          request.pre.account.cash = Math.ceil(request.pre.account.cash/bribeAmount);
          request.pre.account.xp++;
          request.pre.account.heat -= 5;
          if(request.pre.account.heat < 0) { request.pre.account.heat = 0; }
          encounterEvents.push({args: {bribe:true,success:true}, string:"They accepted your bribe! You lost half of your cash, but at least they might leave you alone for a while!"});
        } else {
          encounterEvents.push({args: {bribe:true,success:false}, string:"You tried to bribe them, but you don't have enough cash on you to convince them!"});
        }
      } else if(request.payload.action === "run") {
        var runUpgrade = 0;
        if(hasItem(request.pre.account.inventory,"37")) {
          runUpgrade += 40;
        } else if(hasItem(request.pre.account.inventory,"22")) {
          runUpgrade += 20;
        }
        if(chance.integer({min:1,max:100}) > 60-runUpgrade) {
          isFight = false;
          request.pre.account.opponents = [];
          request.pre.account.heat -= 1;
          if(request.pre.account.heat < 0) { request.pre.account.heat = 0; }
          encounterEvents.push({args: {run:true,success:true}, string:"You got away with everything you were carrying, but it is likely they will find you again soon."});
        } else {
          encounterEvents.push({args: {run:true,success:false}, string:"You tried to run away but they caught up easily!"});
        }
      }

      var opponents = request.pre.account.opponents;

      var newOpponents = [];
      if(isFight) {
        var playerDied = false;
        var whichOpponent = parseInt(request.payload.target,10);
        for(var i=0;i<opponents.length;i++) {

          // player has selected to fight
          if(whichOpponent) {
            if(hasItem(request.pre.account.inventory, "35")) {
              opponents[i].hp -= 4;
              encounterEvents.push({args:{fight:true}, string:"Opponent #"+(i+1) + " LOST 4 HP FROM FEAR DAMAGE!"});
            }              
          }
          // player and all team mates attack the chosen target

          if(i === whichOpponent) {
            if(hasItem(request.pre.account.inventory, "30")) {
              request.pre.account.hp++;
              if(request.pre.account.hp > request.pre.account.maxhp) {
                request.pre.account.hp = request.pre.account.maxhp;
              }
            }                       
            if(hasItem(request.pre.account.inventory, "29")) {
              opponents[i].hp = 0;

              const jediCash = chance.integer({min:100*request.pre.account.level,max:1000*request.pre.account.level});
              request.pre.account.cash += jediCash;
              encounterEvents.push({args:{fight:true}, string:"Opponent #"+(i+1) + " THREW $ " + jediCash + " AT YOU AND RAN OFF!"});
            }
            for(var x=0;x<request.pre.account.teammates.length;x++) {             
              var tW = request.pre.account.teammates[x].weapon;
              if(opponents[i].hp > 0) {
                opponents[i].hp -= attack(tW,opponents[i],"Teammate #"+(x+1),"Opponent #"+(i+1));
              }
              if(hasItem(request.pre.account.inventory, "30")) {
                teammates[x].hp++;
                if(teammates[x].hp > gameConfig.maxhp) {
                  teammates[x].hp = gameConfig.maxhp;
                }
              }
              if(opponents[i].hp <= 0) {
                encounterEvents.push({args:{fight:true}, string:"Opponent #"+(i+1) + " DIED!"});
                break;                         
              }
            }

            if(opponents[i].hp > 0) {
              opponents[i].hp -= attack(request.pre.account.weapon,opponents[i],"You", "Opponent #"+(i+1));
              if(opponents[i].hp <= 0) {
                encounterEvents.push({args:{fight:true}, string:"Opponent #"+(i+1) + " DIED!"});                         
              }
            }
          }
          // this opponent can only attack if it is not dead
          if(opponents[i].hp > 0) {
            // each target attacks a random group member
            var targetString = "You";
            var targetRandom = chance.integer({min:0,max:request.pre.account.teammates.length});

            // select a team mate if possible
            while(hasItem(request.pre.account.inventory,"33") && request.pre.account.teammates.length > 0 && targetRandom === request.pre.account.teammates.length) {
              targetRandom = chance.integer({min:0,max:request.pre.account.teammates.length});
            }
            // index out of bounds, that means player
            if(targetRandom === request.pre.account.teammates.length) {
              if(hasItem(request.pre.account.inventory,"33")) {
                encounterEvents.push({args: {fight:true}, string:"Your opponents cannot see you, but they know you are there."});
              } else {

                var damage = attack(opponents[i].weapon, request.pre.account, "Opponent #"+(i+1), "You");
                request.pre.account.hp -= damage; 
                if(damage > 0) {
                  request.pre.account.encounterShots+=0.1;
                }
                request.pre.account.maxhp = Math.floor(gameConfig.maxhp + request.pre.account.encounterShots);


                if(request.pre.account.hp <= 0) {
                  playerDied = true;
                  // give my weapon back to my inventory
                  console.log(request.pre.account.weapon.code, server.settings.app.weaponsConfig.defaultWeapon.code);
                  if(request.pre.account.weapon.code !== server.settings.app.weaponsConfig.defaultWeapon.code) {
                    request.pre.account.inventory.push({type:'weapon', data: request.pre.account.weapon});
                  }

                  for(var t=0;t<request.pre.account.teammates.length;t++) {                    
                    // get weapons back into inventory before we clear teammates
                    if(request.pre.account.teammates[t].weapon.code !== server.settings.app.weaponsConfig.defaultWeapon.code) {                    
                      request.pre.account.inventory.push({type:'weapon', data: request.pre.account.teammates[t].weapon});
                    }
                  }

                  encounterEvents.push({args: {fight:true,success:false}, string:"You wake up with $100 in your pocket. You don't know how you survived, but now you'll have to start all over."});
                  request.pre.account.hp = server.settings.app.gameConfig.maxhp;
                  request.pre.account.maxhp = server.settings.app.gameConfig.maxhp;
                  request.pre.account.opponents = [];
                  request.pre.account.teammates = [{type:"Mom Dukes", hp:30,weapon:server.settings.app.weaponsConfig.defaultWeapon}];
                  request.pre.account.weapon = server.settings.app.weaponsConfig.defaultWeapon;
                  request.pre.account.encounterShots = 0;
                  request.pre.account.storageLocations = [];
                  request.pre.account.levelPowerupHeat = false,
                  request.pre.account.levelPowerupHp = false;

                  request.pre.account.level = 0;
                  // request.pre.account.xp = 0;
                  request.pre.account.heat = 0;
                  request.pre.account.goods = [];
                  request.pre.account.sourcesList = [];
                  request.pre.account.cash = 100;

                  Account.findById(id, (err, account) => {

                      if (err) {
                          return reply(err);
                      }

                      if (!account) {
                          return reply(Boom.notFound('Document not found. That is strange.'));
                      }
                      account.timeCreated = new Date();
                      delete account._id;
                      delete account.config;
                      delete account.sourcesList;
                      delete account.currentSource;
                      delete account.sourceCash;
                      delete account.sourceGoods;
                      delete account.encounterStatus;
                      Pusher.create(account, function() {
                          // do nothing
                      });
                  });
                  break;
                }
              }
            } else {
              request.pre.account.teammates[targetRandom].hp -= attack(opponents[i].weapon, request.pre.account.teammates[targetRandom], "Opponent #"+(i+1), "Teammate #"+(targetRandom+1));
              if(request.pre.account.teammates[targetRandom].hp <= 0) {
                  if(hasItem(request.pre.account.inventory, "36")) {
                    request.pre.account.teammates[targetRandom].hp = 10;
                    encounterEvents.push({args: {fight:true}, string:"Teammate #" + (targetRandom+1) + " died but you were able to use your wizard powers to bring him back!"});
                  } else {
                    // team mate has died, remove him from the DB
                    // but add the weapon back to inventory first
                    var oldWeapon = {type:"weapon",data:request.pre.account.teammates[targetRandom].weapon};
                    console.log(oldWeapon,request.pre.account.inventory.length);
                    request.pre.account.inventory.push(oldWeapon);
                    console.log(oldWeapon,request.pre.account.inventory.length);
                    request.pre.account.teammates.splice(targetRandom,1);
                    encounterEvents.push({args: {fight:true}, string:"Teammate #" + (targetRandom+1) + " DIED!"});
                  }
              }
            }
          }
        }
        if(!playerDied) {

          for(var deadCheckIndex=0;deadCheckIndex<opponents.length;deadCheckIndex++) {

            if(opponents[deadCheckIndex].hp > 0) {
              newOpponents.push(opponents[deadCheckIndex]);
            }
          }
          if(!newOpponents.length) {
            request.pre.account.xp++;
            if(!hasItem(request.pre.account.inventory,"24")) {
              request.pre.account.heat += 1;
            }
            encounterEvents.push({args: {fight:true,success:true}, string:"YOU HAVE DEFEATED ALL THE OPPONENTS!"});
          }
        }
      }
      request.pre.account.encounterStatus = newOpponents.length + " opponent" + (newOpponents.length>1?"s":"") + " remain"+(newOpponents.length>1?"":"s")+"!";
      request.pre.account.opponents = newOpponents;
      var update = request.pre.account;

      Account.findByIdValidateAndUpdate(id, request.pre.account, update, (err, account) => {

          if (err) {
              return reply(err);
          }
          account.encounterEvents = encounterEvents;
          account.config = gameConfig;
          reply(account);
      });
    };
    server.route({
      method: "POST",
      path: "/buy",
      config: { 
        auth: {
          strategy: 'simple',
          scope: 'account'
        }, pre: [{
          assign: 'account',
          method: getAccount
        // },{
        //   method: function(request,reply) {
        //     var unsoldItemsLength = 0; 
        //     for(var i=0;i<request.pre.account.inventory.length;i++) {
        //       if(!request.pre.account.inventory[i].listDate) {
        //         unsoldItemsLength++;
        //         if(request.payload.itemData.code === request.pre.account.inventory[i].data.code) {
        //           return reply(Boom.badRequest("You already purchased that item, it's in your inventory."));
        //         }
        //       }
        //     }
        //     if(unsoldItemsLength >= 6) {
        //       return reply(Boom.badRequest("You already have 6 or more items. You cannot buy anymore now. Try selling or dropping some items."));
        //     }

        //     return reply(true);
        //   }
        },{
          assign: 'itemConfig',
          method: function(request,reply) {
            var itemConfig = undefined; 
            var allItems = undefined;

            if(request.payload.itemType === 'weapon') {
              var allItems = _.flatten(server.settings.app.weaponsConfig.weaponsByLevel);
            } else if(request.payload.itemType === 'gear') {
              var allItems = _.flatten(server.settings.app.gearConfig.byLevel);
            }

            for(var i=0;i<allItems.length;i++) {
              if(request.payload.itemData.code === allItems[i].code) {
                itemConfig = allItems[i];
              }
            }
            if(!itemConfig) {
              return reply(Boom.badRequest("That item does not exist"));
            }    

            const  inventoryMatch = {
                type: request.payload.itemType,
                listDate: {$exists:true},
                claimDate: {$exists:false},
                "data.code": itemConfig.code
            };             
            return reply({inventoryMatch: inventoryMatch,itemConfig: itemConfig});    

          }
        },{
          assign: 'sellerIndex',
          method: function (request, reply) {
            var whichUser = {
              bitcoin:{$exists:true},
              unconfirmedPurchases: false,
              _id: {$ne: request.pre.account._id},
              inventory: { $elemMatch: request.pre.itemConfig.inventoryMatch}
            };
            Account.count(whichUser, function(err,count) {
              if(err) {
                return reply(Boom.badRequest("cannot count users."));
              }
              if(!count) {
                return reply(Boom.badRequest("Sorry, nobody has that item for sale."));
              }              
              var serverSeed = crypto.randomBytes(16);
              var clientSeeds = {};
              var outcomes = [];
              for(var i=0;i<count;i++) {
                clientSeeds[i] = crypto.randomBytes(16);
                outcomes.push(i);
              }

              var clientSeed = "";
              for(var i=0;i<outcomes.length;i++) {
                var thisSeed = clientSeeds[outcomes[i]];
                clientSeed += thisSeed;
              }

              var initSeed = serverSeed;
              // get random init array, which is shared with the user
              var unshuffled = [];
              for (var i = 0, max = outcomes.length; i < max; i++) {
                  unshuffled.push(i);
              }
              var initArray = provable.seededShuffle(initSeed, unshuffled);
              // hash the init array (comma separated string) with the server seed
              var initArraySha = crypto.createHash('sha256');
              initArraySha.update(JSON.stringify({
                  initialArray: initArray.join(','),
                  serverSeed: serverSeed
              }));
              var initSecretHash = crypto.createHash('sha512');
              initSecretHash.update(serverSeed);
              var shaHash = initArraySha.digest('hex');
              var arrayInit = initArray.join(',');


              var serverArray = provable.seededShuffle(serverSeed, arrayInit.split(','));
              //shuffle the server array with the client seed
              var arrayFinal = provable.seededShuffle(clientSeed, serverArray);
              // result is the first number of the final array
              var result = arrayFinal[0];
              return reply(result);
            });
          }
        },{
          assign: 'seller',
          method: function(request,reply) {
            var whichUser = {
              bitcoin:{$exists:true},
              unconfirmedPurchases: false,
              _id: {$ne: request.pre.account._id},
              inventory: { $elemMatch: request.pre.itemConfig.inventoryMatch}
            };                              
            Account.findOne(whichUser, 
              null, 
              {sort:{timeCreated:1},skip:request.pre.sellerIndex,limit:1}, 
              function(err,seller) {
                if(!seller || err) {
                  return reply(Boom.badRequest("There must have been a program error. We found a seller but then we lost him."));
                }
                var itemDataReal = undefined;
                if(!seller.inventory.length) {
                  return reply(Boom.badRequest("The seller we found does not seem to to have any items."));              
                }
                for(var i=0;i<seller.inventory.length;i++) {
                  if(request.payload.itemData.code === seller.inventory[i].data.code) {
                    itemDataReal = seller.inventory[i];
                    itemDataReal.claimDate = new Date();
                  }
                }
                if(!itemDataReal) {
                  return reply(Boom.badRequest("The seller we found seems to have lost that item. This is probably a program error."));
                }
                Account.findByIdValidateAndUpdate(seller._id,seller,seller,function(err,updatedSeller) {
                  if(err) { 
                    return reply(err);
                  }
                  updatedSeller.itemDataReal = itemDataReal;
                  return reply(updatedSeller);
                });
            });            
          }
        },{
          assign: 'purchase',
          method: function (request,reply) {

            var percentageApp = 0.3;
            if(hasItem(request.pre.account.inventory,"41")) {
              percentageApp = 0.2;
            }
            var satoshiPrice = request.pre.seller.itemDataReal.data.price;
            var satoshiApp = Math.floor(request.pre.seller.itemDataReal.data.price*percentageApp);
            var satoshiSeller = satoshiPrice-satoshiApp-FEE;

            Purchase.count({}, function(err,count) {
              var derivedAddress = new bitcore.Address(new bitcore.HDPublicKey.fromBuffer(XPUBKEY).derive("m/"+count).publicKey,bitcore.Networks.livenet);

              var purchase = {
                timeCreated:  new Date(),
                buyerId: request.pre.account._id.toString(),
                sellerId: request.pre.seller._id.toString(),
                item: request.pre.seller.itemDataReal,
                type: request.pre.seller.itemDataReal.type,
                satoshiPrice: satoshiPrice,
                satoshiApp: satoshiApp,
                satoshiFee: FEE,
                satoshiSeller: satoshiSeller,
                bitcoinAddressApp: derivedAddress.toString(),
                bitcoinAddressAppXPUB: XPUBKEY,
                bitcoinAddressAppCount: count,
                bitcoinAddressSeller: request.pre.seller.bitcoin
              };
              Purchase.validateCreate(purchase,function (err,newPurchase) {
                if(err) {
                  return reply(err);
                }


                return reply(purchase);       
              });
            });
          }
        }]
      },
      handler: function(request,reply) {

        var update = request.pre.account;
        update.purchases.push(request.pre.purchase);
        update.unconfirmedPurchases = true;
        Account.findByIdValidateAndUpdate(request.pre.account._id, update, update, (err, account) => {

            if (err) {
                return reply(err);
            }

            account.config = gameConfig;
            reply(account);
        }); 
      }
    });
    server.route({
      method: "GET",
      path: "/market-list/{type?}",
      config: { 
        auth: {
          strategy: 'simple',
          scope: 'account'
        }
      },
      handler: function(request,reply) {
        if(request.params.type === 'weapon') {
          return reply(_.flatten(server.settings.app.weaponsConfig.weaponsByLevel));
        }
        if(request.params.type === 'gear') {
          return reply(_.flatten(server.settings.app.gearConfig.byLevel));
        }        
        return reply(Boom.badRequest("Cannot find any items of that type"));
      }
    });
    server.route({ 
      method:"POST",
      path:"/drop",
      config: {
        auth: {
            strategy: 'simple',
            scope: 'account'
        },
        pre: [{
            method: getAccount,
            assign:'account'
        }]
      },
      handler: function(request,reply) {

        const id = request.auth.credentials.roles.account._id.toString();
        var update = request.pre.account;

        var newDropped;
        for(var x=0;x<request.pre.account.inventory.length;x++) {
          if(request.pre.account.inventory[x].data.code === request.payload.code) {
            newDropped = request.pre.account.inventory[x];   
            request.pre.account.inventory.splice(x,1);
            break;
          }
        }        
        if(!newDropped) {
          return reply(Boom.badRequest("no such inventory index"));
        }

        Account.findByIdValidateAndUpdate(id, update, update, (err, account) => {

            if (err) {
                return reply(err);
            }
            account.config = gameConfig;
            reply(account);
        });

      }
    });
    server.route({
      method:"POST",
      path:"/sell",
      config: {
        auth: {
            strategy: 'simple',
            scope: 'account'
        },
        pre: [{
            method: getAccount,
            assign:'account'
        }]
      },
      handler: function(request,reply) {

        const id = request.auth.credentials.roles.account._id.toString();

        if (!request.pre.account.bitcoin || !bitcore.Address.isValid(request.pre.account.bitcoin, bitcore.Networks.livenet)) {
          return reply(Boom.badRequest("You cannot use this feature! Make a new account with a bitcoin address to use it!"));
        }
        var update = request.pre.account;
        var newSold;
        for(var x=0;x<request.pre.account.inventory.length;x++) {
          if(request.pre.account.inventory[x].data.code === request.payload.code) {
            newSold = request.pre.account.inventory[x];   
            request.pre.account.inventory[x].listDate = new Date();
            break;
          }
        }        
        if(!newSold) {
          return reply(Boom.badRequest("That item is not in your inventory."));
        }
        // mark a sold date for no reason really
        for(var i=0;i<request.pre.account.purchases.length;i++) {
          if(request.pre.account.purchases[i].item.data.code === newSold.data.code) {
            request.pre.account.purchases[i].item.soldDate = new Date();
            break;
          }
        }        

        Account.findByIdValidateAndUpdate(id, update, update, (err, account) => {

            if (err) {
                return reply(err);
            }
            account.config = gameConfig;
            reply(account);
        });
      }
    });

    server.route({
      method:"POST",
      path:"/equip",
      config: {
        auth: {
            strategy: 'simple',
            scope: 'account'
        },
        pre: [{
            method: getAccount,
            assign:'account'
        }]
      },
      handler: function(request,reply) {

        const id = request.auth.credentials.roles.account._id.toString();

        var newEquipped;
        for(var x=0;x<request.pre.account.inventory.length;x++) {
          if(request.pre.account.inventory[x].data.code === request.payload.code) {
            newEquipped = request.pre.account.inventory[x];   
            request.pre.account.inventory.splice(x,1);
            break;
          }
        }
        if(!newEquipped) {
          return reply(Boom.badRequest("That item is not in your inventory."));          
        }
        if(newEquipped.listDate) {
          return reply(Boom.badRequest("You listed that item for sale. It's gone now."));
        }
        if(newEquipped.type !== 'weapon') {
          return reply(Boom.badRequest("Your gear is always equipped. Equip your weapons only!"));
        }

        if(request.payload.teamindex === -1) {
          var newInventory = {
            type: "weapon",
            data: request.pre.account.weapon
          };
          request.pre.account.inventory.push(newInventory); 
          request.pre.account.weapon = newEquipped.data;             
        } else {

          var newInventory = {
            type: "weapon",
            data: request.pre.account.teammates[request.payload.teamindex].weapon
          };
          request.pre.account.inventory.push(newInventory);

          request.pre.account.teammates[request.payload.teamindex].weapon = newEquipped.data;
        }

        var update = request.pre.account;

        Account.findByIdValidateAndUpdate(id, request.pre.account, update, (err, account) => {

            if (err) {
                return reply(err);
            }
            account.config = gameConfig;
            reply(account);
        });
      }
    });
    server.route({
      method: "POST",
      path: "/encounter",
      config: {
        auth: {
            strategy: 'simple',
            scope: 'account'
        },
        pre: [{
            method: getAccount,
            assign:'account'
        },{
          method: encounterResponse,
          assign: 'encounterResponse'
        }]
      },
      handler: function (request, reply) {
        reply(request.pre.encounterResponse);
      }
    });
    const clearSource = function(request,reply) {

        const id = request.auth.credentials.roles.account._id.toString();

        const update = {};
        update.$set = {
          sourceGoods: [],
          sourceCash: 0,
          currentSource: null
        };

        Account.findByIdAndUpdate(id, update, (err, account) => {

            if (err) {
                return reply(err);
            }

            reply(account);
        });
    };
    server.route({
        path: '/game-source-list',
        method: "GET",
        config: {
            auth: {
                strategy: 'simple',
                scope: 'account'
            },
            pre: [{
                method: getAccount,
                assign: 'account'
            }, {
                assign: 'account',
                method: function (request, reply) {

                    var maxSources = 6;
                    if(request.pre.account.sourcesList.length === maxSources) {
                      return reply(request.pre.account);
                    }
                    const id = request.auth.credentials.roles.account._id.toString();
                    Account.findById(id, (err, account) => {

                        if (err) {
                            return reply(err);
                        }

                        if (!account) {
                            return reply(Boom.notFound('Document not found.'));
                        }
                        if(account.sourcesList.length > maxSources) {
                          return reply(Boom.badRequest('max sources constraint'));
                        }
                        if(account.sourcesList.length < maxSources) {
                          var newSourcesArray = [];
                          var addSources = maxSources - account.sourcesList.length;

                          var goodsArray = [];
                          for(var i in gameConfig.goodsPrices) {
                            goodsArray.push(i);
                          }
                          var randomAvatars = [];
                          while(randomAvatars.length < addSources) {
                            randomAvatars.push(chance.integer({min:1,max:9})+".png");
                            randomAvatars = _.uniq(randomAvatars);
                          }
                          while(addSources > newSourcesArray.length) {
                            var newIndex = account.sourcesList.length+newSourcesArray.length+1;
                            // every source is just a generic one for now
                            var prefBuy = goodsArray[chance.integer({min:0,max:5})];
                            var prefSell = goodsArray[chance.integer({min:0,max:5})];
                            while (prefBuy === prefSell) {
                              prefBuy = goodsArray[chance.integer({min:0,max:5})];
                            }

                            newSourcesArray.push({
                              sourceNumber: newIndex,
                              timeCreated: new Date(),
                              type: "source",
                              prefBuy: prefBuy,
                              prefSell: prefSell,
                              avatar: {
                                graphic: randomAvatars[newSourcesArray.length],
                                name: chance.first(),
                                location: chance.state({ us_states_and_dc: true, full: true })// nationality: "en"
                              }
                            });
                          }
                          const update = {};
                          update.$push = {
                            sourcesList: {
                              $each: newSourcesArray
                            }
                          };

                          Account.findByIdValidateAndUpdate(id, account, update, (err, account) => {

                              if (err) {
                                  return reply(err);
                              }

                              reply(account);
                          });
                        } else {
                          reply(account);
                        }
                    });
                }
            }]
        },
        handler: function (request, reply) {

            const id = request.auth.credentials.roles.account._id.toString();

            Account.findById(id, (err, account) => {

                if (err) {
                    return reply(err);
                }

                if (!account) {
                    return reply(Boom.notFound('Document not found. That is strange.'));
                }
                account.config = gameConfig;

                if(hasItem(account.inventory,"40")) {
                  account.cash += Math.floor(account.sourceCash*50);
                  account.sourceCash -= Math.floor(account.sourceCash*50);
                } 
                if(hasItem(account.inventory,"32")) {
                  account.cash += Math.floor(account.sourceCash*10);
                  account.sourceCash -= Math.floor(account.sourceCash*10);
                }
                account.sourceGoods = [];
                account.currentSource = [];
                account.sourceCash = 0;
                reply(account);
            });
        }
    });


    server.route({
        method: 'GET',
        path: '/accounts',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'admin'
            },
            validate: {
                query: {
                    fields: Joi.string(),
                    sort: Joi.string().default('_id'),
                    limit: Joi.number().default(20),
                    page: Joi.number().default(1)
                }
            }
        },
        handler: function (request, reply) {

            const query = {};
            const fields = request.query.fields;
            const sort = request.query.sort;
            const limit = request.query.limit;
            const page = request.query.page;

            Account.pagedFind(query, fields, sort, limit, page, (err, results) => {

                if (err) {
                    return reply(err);
                }

                reply(results);
            });
        }
    });


    server.route({
        method: 'GET',
        path: '/accounts/{id}',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'admin'
            }
        },
        handler: function (request, reply) {

            Account.findById(request.params.id, (err, account) => {

                if (err) {
                    return reply(err);
                }

                if (!account) {
                    return reply(Boom.notFound('Document not found.'));
                }

                reply(account);
            });
        }
    });

    server.route({
        method: 'POST',
        path: '/accounts',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'admin'
            },
            validate: {
                payload: {
                    name: Joi.string().required()
                }
            }
        },
        handler: function (request, reply) {

            const name = request.payload.name;

            Account.create(name, (err, account) => {

                if (err) {
                    return reply(err);
                }

                reply(account);
            });
        }
    });


    server.route({
        method: 'PUT',
        path: '/accounts/{id}',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'admin'
            },
            validate: {
                payload: {
                    name: Joi.string().required()
                }
            }
        },
        handler: function (request, reply) {

            const id = request.params.id;
            const update = {
                $set: {
                    name: request.payload.name
                }
            };

            Account.findByIdAndUpdate(id, update, (err, account) => {

                if (err) {
                    return reply(err);
                }

                if (!account) {
                    return reply(Boom.notFound('Document not found.'));
                }

                reply(account);
            });
        }
    });


    server.route({
        method: 'PUT',
        path: '/accounts/my',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'account'
            },
            validate: {
                payload: {
                    name: Joi.string().required()
                }
            }
        },
        handler: function (request, reply) {

            const id = request.auth.credentials.roles.account._id.toString();
            const update = {
                $set: {
                    name: request.payload.name
                }
            };
            const findOptions = {
                fields: Account.fieldsAdapter('user name timeCreated game')
            };

            Account.findByIdAndUpdate(id, update, findOptions, (err, account) => {

                if (err) {
                    return reply(err);
                }

                reply(account);
            });
        }
    });


    server.route({
        method: 'PUT',
        path: '/accounts/{id}/user',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'admin'
            },
            validate: {
                payload: {
                    username: Joi.string().lowercase().required()
                }
            },
            pre: [{
                assign: 'account',
                method: function (request, reply) {

                    Account.findById(request.params.id, (err, account) => {

                        if (err) {
                            return reply(err);
                        }

                        if (!account) {
                            return reply(Boom.notFound('Document not found.'));
                        }

                        reply(account);
                    });
                }
            }, {
                assign: 'user',
                method: function (request, reply) {

                    User.findByUsername(request.payload.username, (err, user) => {

                        if (err) {
                            return reply(err);
                        }

                        if (!user) {
                            return reply(Boom.notFound('User document not found.'));
                        }

                        if (user.roles &&
                            user.roles.account &&
                            user.roles.account.id !== request.params.id) {

                            return reply(Boom.conflict('User is already linked to another account. Unlink first.'));
                        }

                        reply(user);
                    });
                }
            }, {
                assign: 'userCheck',
                method: function (request, reply) {

                    if (request.pre.account.user &&
                        request.pre.account.user.id !== request.pre.user._id.toString()) {

                        return reply(Boom.conflict('Account is already linked to another user. Unlink first.'));
                    }

                    reply(true);
                }
            }]
        },
        handler: function (request, reply) {

            Async.auto({
                account: function (done) {

                    const id = request.params.id;
                    const update = {
                        $set: {
                            user: {
                                id: request.pre.user._id.toString(),
                                name: request.pre.user.username
                            }
                        }
                    };

                    Account.findByIdAndUpdate(id, update, done);
                },
                user: function (done) {

                    const id = request.pre.user._id;
                    const update = {
                        $set: {
                            'roles.account': {
                                id: request.pre.account._id.toString(),
                                name: request.pre.account.name
                            }
                        }
                    };

                    User.findByIdAndUpdate(id, update, done);
                }
            }, (err, results) => {

                if (err) {
                    return reply(err);
                }

                reply(results.account);
            });
        }
    });


    server.route({
        method: 'DELETE',
        path: '/accounts/{id}/user',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'admin'
            },
            pre: [{
                assign: 'account',
                method: function (request, reply) {

                    Account.findById(request.params.id, (err, account) => {

                        if (err) {
                            return reply(err);
                        }

                        if (!account) {
                            return reply(Boom.notFound('Document not found.'));
                        }

                        if (!account.user || !account.user.id) {
                            return reply(account).takeover();
                        }

                        reply(account);
                    });
                }
            }, {
                assign: 'user',
                method: function (request, reply) {

                    User.findById(request.pre.account.user.id, (err, user) => {

                        if (err) {
                            return reply(err);
                        }

                        if (!user) {
                            return reply(Boom.notFound('User document not found.'));
                        }

                        reply(user);
                    });
                }
            }]
        },
        handler: function (request, reply) {

            Async.auto({
                account: function (done) {

                    const id = request.params.id;
                    const update = {
                        $unset: {
                            user: undefined
                        }
                    };

                    Account.findByIdAndUpdate(id, update, done);
                },
                user: function (done) {

                    const id = request.pre.user._id.toString();
                    const update = {
                        $unset: {
                            'roles.account': undefined
                        }
                    };

                    User.findByIdAndUpdate(id, update, done);
                }
            }, (err, results) => {

                if (err) {
                    return reply(err);
                }

                reply(results.account);
            });
        }
    });


    server.route({
        method: 'POST',
        path: '/accounts/{id}/notes',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'admin'
            },
            validate: {
                payload: {
                    data: Joi.string().required()
                }
            }
        },
        handler: function (request, reply) {

            const id = request.params.id;
            const update = {
                $push: {
                    notes: {
                        data: request.payload.data,
                        timeCreated: new Date(),
                        userCreated: {
                            id: request.auth.credentials.user._id.toString(),
                            name: request.auth.credentials.user.username
                        }
                    }
                }
            };

            Account.findByIdAndUpdate(id, update, (err, account) => {

                if (err) {
                    return reply(err);
                }

                reply(account);
            });
        }
    });


    server.route({
        method: 'POST',
        path: '/accounts/{id}/status',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'admin'
            },
            validate: {
                payload: {
                    status: Joi.string().required()
                }
            },
            pre: [{
                assign: 'status',
                method: function (request, reply) {

                    Status.findById(request.payload.status, (err, status) => {

                        if (err) {
                            return reply(err);
                        }

                        reply(status);
                    });
                }
            }]
        },
        handler: function (request, reply) {

            const id = request.params.id;
            const newStatus = {
                id: request.pre.status._id.toString(),
                name: request.pre.status.name,
                timeCreated: new Date(),
                userCreated: {
                    id: request.auth.credentials.user._id.toString(),
                    name: request.auth.credentials.user.username
                }
            };
            const update = {
                $set: {
                    'status.current': newStatus
                },
                $push: {
                    'status.log': newStatus
                }
            };

            Account.findByIdAndUpdate(id, update, (err, account) => {

                if (err) {
                    return reply(err);
                }

                reply(account);
            });
        }
    });


    server.route({
        method: 'DELETE',
        path: '/accounts/{id}',
        config: {
            auth: {
                strategy: 'simple',
                scope: 'admin'
            },
            pre: [
                AuthPlugin.preware.ensureAdminGroup('root')
            ]
        },
        handler: function (request, reply) {

            Account.findByIdAndDelete(request.params.id, (err, account) => {

                if (err) {
                    return reply(err);
                }

                if (!account) {
                    return reply(Boom.notFound('Document not found.'));
                }

                reply({ message: 'Success.' });
            });
        }
    });


    next();
};


exports.register = function (server, options, next) {

    server.dependency(['auth', 'hapi-mongo-models'], internals.applyRoutes);

    next();
};


exports.register.attributes = {
    name: 'account'
};
