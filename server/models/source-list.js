'use strict';

const Joi = require('joi');
const ObjectAssign = require('object-assign');
const BaseModel = require('hapi-mongo-models').BaseModel;
const Goods = require('./goods');

const SourceList = BaseModel.extend({
    constructor: function (attrs) {

        ObjectAssign(this, attrs);
    }
});

SourceList.schema = Joi.object().keys({
    sourceNumber: Joi.number().integer().positive().required(),
    timeCreated: Joi.date().required(),
    type: Joi.string().required(),
    prefBuy: Joi.string().required(),
    prefSell: Joi.string().required(),
    // trust: Joi.number().max(10).positive().integer().required(),
    // level: Joi.number().max(10).positive().integer().required(),
    avatar: Joi.object().keys({
      graphic: Joi.string().required(),
      location: Joi.string().required(),
      name: Joi.string().required()
    }).required()
});




module.exports = SourceList;
