'use strict';

const Joi = require('joi');
const ObjectAssign = require('object-assign');
const BaseModel = require('hapi-mongo-models').BaseModel;

const InventoryItem = BaseModel.extend({
    constructor: function (attrs) {

        ObjectAssign(this, attrs);
    }
});

InventoryItem.schema = Joi.object().keys({
    data: Joi.object().required(),
    type: Joi.string().required(),
    amount: Joi.number().positive().integer(),
    listDate: Joi.date(),
    claimDate: Joi.date()
});


module.exports = InventoryItem;
