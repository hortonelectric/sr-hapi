'use strict';

const Joi = require('joi');
const ObjectAssign = require('object-assign');
const BaseModel = require('hapi-mongo-models').BaseModel;
const Weapon = require('./weapon');
const Npc = BaseModel.extend({
    constructor: function (attrs) {
        ObjectAssign(this, attrs);
    }
});

Npc.schema = Joi.object().keys({
    hp: Joi.number().integer().positive(),
    weapon: Weapon.schema,
    type: Joi.string(),
    armor: Joi.number().integer().positive()
});


module.exports = Npc;
