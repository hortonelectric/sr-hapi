'use strict';

const Account = require('./account');

const Pusher = Account.extend();


Pusher._collection = 'pushers';

Pusher.indexes = [
    { key: { 'level': 1 } },
    { key: { 'cash': 1 } },
    { key: { 'xp' : 1 } }
];


Pusher.create = function (document, callback) {


    this.insertOne(document, (err, docs) => {

        if (err) {
            return callback(err);
        }

        callback(null, docs[0]);
    });
};

module.exports = Pusher;
