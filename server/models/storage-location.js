'use strict';

const Joi = require('joi');
const ObjectAssign = require('object-assign');
const BaseModel = require('hapi-mongo-models').BaseModel;

const InventoryItem = require('./inventory-item');
const Goods = require('./goods');

const StorageLocation = BaseModel.extend({
    constructor: function (attrs) {

        ObjectAssign(this, attrs);
    }
});


StorageLocation.schema = Joi.object().keys({
    name: Joi.string().required(),
    timeCreated: Joi.date().required(),
    cash: Joi.number().positive().precision(2),
    inventory: Joi.array().items(InventoryItem.schema),
    goods: Joi.array().items(Goods.schema),
    capacity: Joi.number().positive().integer()
});


module.exports = StorageLocation;
