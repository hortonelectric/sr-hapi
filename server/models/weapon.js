'use strict';

const Joi = require('joi');
const ObjectAssign = require('object-assign');
const BaseModel = require('hapi-mongo-models').BaseModel;

const Weapon = BaseModel.extend({
    constructor: function (attrs) {
        ObjectAssign(this, attrs);
    }
});

Weapon.schema = Joi.object().keys({
	price: Joi.number().integer().required(),
    name:Joi.string().required(),
    code:Joi.string().required(),
    type:Joi.string(),
    consumable:Joi.boolean(),
    damageModifier: Joi.number().positive().integer().required(),
    damageClass: Joi.number().positive().integer().required(),
    hitModifier: Joi.number().positive().integer().required()
});


module.exports = Weapon;
