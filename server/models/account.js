'use strict';

const Joi = require('joi');
const ObjectAssign = require('object-assign');
const BaseModel = require('hapi-mongo-models').BaseModel;
const StatusEntry = require('./status-entry');
const StorageLocation = require('./storage-location');
const Ability = require('./ability');
const SourceList = require('./source-list');
const Goods = require('./goods');
const InventoryItem = require('./inventory-item');
const Npc = require('./npc');
const Weapon = require('./weapon');
const Purchase = require('./purchase');
const NoteEntry = require('./note-entry');

const Account = BaseModel.extend({
    constructor: function (attrs) {

        ObjectAssign(this, attrs);
    }
});


Account._collection = 'accounts';


Account.schema = Joi.object().keys({
    _id: Joi.object(),
    user: Joi.object().keys({
        id: Joi.string().required(),
        name: Joi.string().lowercase().required()
    }),
    name: Joi.string().required(),
    status: Joi.object().keys({
        current: StatusEntry.schema,
        log: Joi.array().items(StatusEntry.schema)
    }),
    notes: Joi.array().items(NoteEntry.schema),
    verification: Joi.object().keys({
        complete: Joi.boolean(),
        token: Joi.string()
    }),

    bitcoin: Joi.string().allow(''),
    timeCreated: Joi.date(),
    level: Joi.number().min(0).integer(),
    nextLevel: Joi.number().integer(),
    nextLevelCost: Joi.number().integer(),
    levelPowerupHp: Joi.boolean(),
    levelPowerupHeat: Joi.boolean(),
    cash: Joi.number().min(0).integer(),
    storageLocations: Joi.array().items(StorageLocation.schema),
    heat: Joi.number().precision(2).min(0).required(),
    hp: Joi.number().integer().min(0).required(),
    maxhp: Joi.number().positive().integer(),
    hold: Joi.number().positive().integer(),
    gametime: Joi.number().integer().min(0),
    xp: Joi.number().integer().min(0),
    weapon: Weapon.schema,
    armor: Joi.number().positive().integer(),
    encounterStatus: Joi.string().allow(''),
    encounterShots: Joi.number().min(0).precision(2),
    abilities: Joi.array().items(Ability.schema),
    teammates: Joi.array().items(Npc.schema),
    inventory: Joi.array().items(InventoryItem.schema),
    purchases: Joi.array().items(Purchase.schema),
    itemsSold: Joi.array().items(Purchase.schema),
    unconfirmedPurchases: Joi.boolean(),
    goods: Joi.array().items(Goods.schema),
    sourcesList: Joi.array().items(SourceList.schema),
    currentSource: SourceList.schema,
    sourceGoods: Joi.array().items(Goods.schema),
    sourceCash: Joi.number().integer().min(0),
    sourceNumber: Joi.number().integer().min(1),
    opponents: Joi.array().items(Npc.schema)
});


Account.indexes = [
    { key: { 'user.id': 1 } },
    { key: { 'user.name': 1 } },
    { key: { 'unconfirmedPurchases': 1 } },
    { key: { 'inventory': 1 } },
    { key: { 'inventory.type': 1 } },
    { key: { 'inventory.data.code': 1 } },
    { key: { 'inventory.listDate': -1 } },
    { key: { 'inventory.claimDate': 1 } },
    { key: { 'purchases.timeConfirmed': -1 } },
    { key: { 'purchases.bitcoinAddressApp': -1 } },
    { key: { 'purchases.timeCreated': -1 } },
    { key: { 'level': -1 } },
    { key: { 'bitcoin': 1 } }
];


Account.create = function (name, bitcoin, callback) {
    const defaultWeapon = {type:"weapon",code:"0",price:100000,name:"Knife",damageModifier: 1, hitModifier: 1, damageClass: 4};

     const document = {
        name: name,
        bitcoin: bitcoin||'',
        timeCreated: new Date(),
        level: 0,
        cash: 100,
        armor: 10,
        storageLocations: [],
        heat: 0,
        unconfirmedPurchases:false,
        teammates: [{type:"N00b", hp:30,weapon:defaultWeapon}],
        encounterShots: 0,
        encounterStatus: "",
        weapon: defaultWeapon,
        hp: 30,
        maxhp: 30,
        xp: 0,
        gametime: 0,
        abilities: [],
        inventory: [],
        purchases: [],
        goods: [],
        itemsSold: [],
        sourcesList: [],
        opponents: []
    };

    this.insertOne(document, (err, docs) => {

        if (err) {
            return callback(err);
        }

        callback(null, docs[0]);
    });
};


Account.findByUsername = function (username, callback) {

    const query = { 'user.name': username.toLowerCase() };
    this.findOne(query, callback);
};

Account.findByIdValidateAndUpdate = function(id,account,update,reply) {

    Joi.validate(account, Account.schema, function (errValidate, value) { 
        if (errValidate) {
            return reply(errValidate);
        }

        Account.findByIdAndUpdate(id, update, (err, account) => {

          if (err) {
              return reply(err);
          }
          reply(null,account);
        });
    });
}

module.exports = Account;
