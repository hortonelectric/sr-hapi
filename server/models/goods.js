'use strict';

const Joi = require('joi');
const ObjectAssign = require('object-assign');
const BaseModel = require('hapi-mongo-models').BaseModel;

const Goods = BaseModel.extend({
    constructor: function (attrs) {

        ObjectAssign(this, attrs);
    }
});


Goods.schema = Joi.object().keys({
    type: Joi.string().required(),
    amount: Joi.number().min(0).integer(),
    price: Joi.number().positive().integer()
});


module.exports = Goods;
