'use strict';

const Joi = require('joi');
const ObjectAssign = require('object-assign');
const BaseModel = require('hapi-mongo-models').BaseModel;

const Ability = BaseModel.extend({
    constructor: function (attrs) {

        ObjectAssign(this, attrs);
    }
});

Ability.schema = Joi.object().keys({
    name: Joi.string().required(),
    timeCreated: Joi.date().required(),
    type: Joi.string().required(),
    amount: Joi.number().positive().integer()
});


module.exports = Ability;
