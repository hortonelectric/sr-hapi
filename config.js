'use strict';

const Confidence = require('confidence');


const criteria = {
    env: process.env.NODE_ENV
};


const config = {
    $meta: 'This file configures silk road.',
    projectName: 'silkroad',
    port: {
        web: {
            $filter: 'env',
            test: 9000,
            $default: 8000
        }
    },
    authAttempts: {
        forIp: 50,
        forIpAndUser: 7
    },
    hapiMongoModels: {
        $filter: 'env',
        production: {
            mongodb: {
                url: 'mongodb://localhost:27017/silkroad'
            },
            autoIndex: false
        },
        test: {
            mongodb: {
                url: 'mongodb://localhost:27017/silkroad-test'
            },
            autoIndex: true
        },
        $default: {
            mongodb: {
                url: 'mongodb://localhost:27017/silkroad'
            },
            autoIndex: true
        }
    },
    nodemailer: {
        host: 's6-vancouver.accountservergroup.com',
        port: 465,
        secure: true,
        auth: {
            user: 'webmaster@6flag.ventures',
            pass: '144m201212'
        }
    },
    system: {
        fromAddress: {
            name: 'silkroad',
            address: 'hortonelectric+silkroad@gmail.com'
        },
        toAddress: {
            name: 'silkroad',
            address: 'hortonelectric+silkroad@gmail.com'
        }
    }
};


const store = new Confidence.Store(config);


exports.get = function (key) {

    return store.get(key, criteria);
};


exports.meta = function (key) {

    return store.meta(key, criteria);
};
