'use strict';

const Confidence = require('confidence');
const Config = require('./config');


const criteria = {
    env: process.env.NODE_ENV
};

const gameConfig = {
  powerupCost: 100000,
  powerupFactor: 1.5,
  priceLevelFactor: 1.15,
  defaultHold: 50,
  defaultHp: 30,
  goodsPrices: {
    "OXY": 20,
    "MOLLY": 80,
    "ICE": 480,
    "WAX": 1600,
    "SMACK": 6400,
    "BLOW": 15000
  },
  maxhp: 30,
  currentVersion: "0.0.1",
  minBribeCash: 2000,
  bribeLevelFactor: 10,
  levels: [
    {xp:0,cash:0,level:0},
    {xp:50,cash:50000,level:1},
    {xp:100,cash:250000,level:2},
    {xp:200,cash:1000000,level:3},
    {xp:350,cash:5000000,level:4},
    {xp:600,cash:20000000,level:5},
    {xp:1000,cash:50000000,level:6},
    {xp:1300,cash:200000000,level:7},
    {xp:1800,cash:600000000,level:8},
    {xp:2400,cash:120000000,level:9},
    {xp:3000,cash:240000000,level:10}
  ]
};
const weaponsConfig = {
  defaultWeapon: {type:"weapon",code:"0",price:100000,name:"Knife",damageModifier: 1, hitModifier: 1, damageClass: 4},
  weaponsByLevel: [
    //0
    [
        {
        type:"weapon",code:"0",price: 10000, name:"Knife",damageModifier: 2, hitModifier: 4, damageClass: 4
        }
    ], [
        {
        type:"weapon",code:"01",price: 50000, name:"Crowbar",damageModifier: 1, hitModifier: 3, damageClass: 4
        }, {
        type:"weapon",code:"02",price: 60000, name:".22 Rifle",damageModifier: 2, hitModifier: 3, damageClass: 4
        }
    ], [
        {
        type:"weapon",code:"03",price: 70000, name:"Katana",damageModifier: 4, hitModifier: 1, damageClass: 4
        }, 
        {
        type:"weapon",code:"04",price: 70000, name:"Colt .45",damageModifier: 3, hitModifier: 2, damageClass: 6
        }
    ],[
        {
        type:"weapon",code:"05",price: 80000, name:".38 Special",damageModifier: 2, hitModifier: 2, damageClass: 4
        },{
        type:"weapon",code:"06",price: 80000, name:"Molotov Cocktail",damageModifier: 6, hitModifier: 5, damageClass: 4, consumable: true
        }
    ],[
        {
        type:"weapon",code:"07",price: 90000, name:".410 Shotgun",damageModifier: 2, hitModifier: 4, damageClass: 6
        }, {
        type:"weapon",code:"08",price: 90000, name:"Glock Semi-Auto",damageModifier: 1, hitModifier: 4, damageClass: 6
        }
    ],[
        {
        type:"weapon",code:"09",price: 100000, name:"Bazooka",damageModifier: 1, hitModifier: 6, damageClass: 10
        },{
        type:"weapon",code:"10",price: 100000, name:"Laser",damageModifier: 1, hitModifier: 8, damageClass: 8
        }
    ],[
        {
        type:"weapon",code:"11",price: 200000, name:"Sniper Rifle",damageModifier: 3, hitModifier: 6, damageClass: 8
        }, {
        type:"weapon",code:"12",price: 200000, name:"AK-47",damageModifier: 4, hitModifier: 4, damageClass: 10
        }
    ],[
        {
        type:"weapon",code:"13",price: 300000, name:"Ninja Throwing Star",damageModifier: 1, hitModifier: 10, damageClass: 4,consumable: true
        },{
        type:"weapon",code:"14",price: 300000, name:"Grenade",damageModifier: 1, hitModifier: 8, damageClass: 12,consumable: true
        }
    ],[
        {
        type:"weapon",code:"15",price: 400000, name:"Pet Zombie",damageModifier: 5, hitModifier: 8, damageClass: 6
        }, {
        type:"weapon",code:"16",price: 400000, name:"Greek Fire",damageModifier: 3, hitModifier: 4, damageClass: 12
        }
    ],[
        {
        type:"weapon",code:"17",price: 800000, name:"Mind Bullets",damageModifier: 2, hitModifier: 12, damageClass: 6
        },{
        type:"weapon",code:"18",price: 1000000, name:"Harry's Wand",damageModifier: 10, hitModifier: 2, damageClass: 8
        }
    ],[
        {
        type:"weapon",code:"19",price: 1200000, name:"Attack Robot",damageModifier: 4, hitModifier: 8, damageClass: 12
        }, {
        type:"weapon",code:"20",price: 1200000, name:"Thor's Hammer",damageModifier: 8, hitModifier: 12, damageClass: 12
        }
    ]     
  ]
};
const gearConfig = {
  byLevel: [
    //0
    [
        {
        type:"gear",code:"21",price: 100000000000, name:"Get out of the Game",description: "Your boss says you don't have to push anymore!"
        }
    ], [
        {
        type:"gear",code:"22",price: 50000, name:"Running Shoes",description: "+20% (total 60%) chance to escape when you select 'RUN'"
        }, {
        type:"gear",code:"23",price: 60000, name:"Welfare Check",description: "Collect $100 per level every time you visit a source."
        }
    ], [
        {
        type:"gear",code:"24",price: 70000, name:"Teen Spirit",description: "Killing your enemies in an encounter no longer raises your notoriety"
        }, {
        type:"gear",code:"25",price: 70000, name:"Mob Bones",description: "Double XP from every deal"
        }, {
        type:"gear",code:"43",price: 70000, name:"McFly's Vest",description: "+2 Armor Class"
        }
    ],[
        {
        type:"gear",code:"26",price: 80000, name:"Vito's Hat",description: "+20% (total 90%) chance to succeed when you select 'BRIBE'"
        },{
        type:"gear",code:"27",price: 80000, name:"Piccard's Heart",description: "Gain an additional 1hp per visit to a source"
        },{
        type:"gear",code:"28",price: 90000, name:"Fountain of Youth",description: "Use your HP powerup infinite times per level"
        }
    ],[
        {
        type:"gear",code:"29",price: 100000, name:"Jedi Mind Trick",description: "If you select 'ATTACK', your enemies lose their senses, running and leaving behind some cash."
        }, {
        type:"gear",code:"30",price: 100000, name:"Wolverine",description: "Regenerates 1 HP per round for you and your teammates during a fight"
        }, {
        type:"gear",code:"44",price: 120000, name:"Neo's Reflexes",description: "+4 Armor Class"
        }
    ],[
        {
        type:"gear",code:"31",price: 150000, name:"The Family Name",description: "Bribe only costs you 25% instead of 50% of your cash"
        },{
        type:"gear",code:"32",price: 160000, name:"Vincent Vega's Jacket",description: "You look so cool in your jacket that your sources give you 10% of their cash whenever you leave"
        }
    ],[
        {
        type:"gear",code:"33",price: 200000, name:"Bilbo's Ring",description: "Your enemies only attack your teammates. If you don't have any teammtes, your enemies wander off in confusion."
        }, {
        type:"gear",code:"34",price: 300000, name:"Hermione's Handbag",description: "Enough space to hide infinite goods!"
        }, {
        type:"gear",code:"45",price: 400000, name:"Iron Man",description: "+6 Armor Class"
        }
    ],[
        {
        type:"gear",code:"35",price: 500000, name:"Scarecrow's Fear Gas",description: "All enemies lose 4 HP every time you attack"
        },{
        type:"gear",code:"36",price: 600000, name:"Horcrux",description: "When a teammate dies, he is given back 10 HP"
        }
    ],[
        {
        type:"gear",code:"37",price: 700000, name:"Batman's Grapnel Gun",description: "+40% (total 80%) chance to escape when you select 'RUN'"
        }, {
        type:"gear",code:"38",price: 800000, name:"Mexican Mistress",description: "Use your 'lay low' powerup infinite times per level"
        }
    ],[
        {
        type:"gear",code:"39",price: 1000000, name:"Trust Fund Baby",description: "Collect $10000 per player level every time you visit a source."
        },{
        type:"gear",code:"40",price: 1000000, name:"Godfather's Suit",description: "As a sign of respect for the family, your sources give you 50% of their cash whenever you leave"
        }
    ],[
        {
        type:"gear",code:"41",price: 1200000, name:"Silk Road Fanatic",description: "Receive +10% (total 80%) of the bitcoin from items sold by you"
        }, {
        type:"gear",code:"42",price: 1200000, name:"Sorcerer's Stone",description: "Gain additional 5 HP every time you visit a source."
        }
    ]     
  ]
}
const manifest = {
    $meta: 'This file defines the silk road.',
    server: {
        debug: {
            request: ['error']
        },
        connections: {
            routes: {
                security: true
            }
        },
        app: {
          gameConfig: gameConfig,
          weaponsConfig: weaponsConfig,
          gearConfig: gearConfig
        }
    },
    connections: [{
        port: Config.get('/port/web'),
        labels: ['web']
    }],
    registrations: [
        {
            plugin: 'hapi-auth-basic'
        },
        {
            plugin: 'lout'
        },
        {
            plugin: 'inert'
        },
        {
            plugin: 'vision'
        },
        {
            plugin: {
                register: 'hapi-job-queue',
                options: {
                    connectionUrl: "mongodb://localhost:27017/silkroad",

                    jobs: [
                      {
                        name: 'check-transactions',
                        enabled: true,
                        schedule: "every 5 minutes",
                        concurrentTasks:1,
                        method: 'checkTransactions', //server method 
                        tasks: [ // each task will run with the task as the data property 
                          {
                            group: 'pendingTransactions'
                          }
                        ]
                      }
                    ]
                }
            }
        },
        {
            plugin: {
                register: 'good',
                options: {
                    ops: {
                        interval: 60000
                    },
                    reporters: {
                        myConsoleReporter: [{
                            module: 'good-squeeze',
                            name: 'Squeeze',
                            args: [{ log: '*', response: '*' }]
                        }, {
                            module: 'good-console'
                        }, 'stdout'],
                        myFileReporter: [{
                            module: 'good-squeeze',
                            name: 'Squeeze',
                            args: [{ ops: '*' }]
                        }, {
                            module: 'good-squeeze',
                            name: 'SafeJson'
                        }, {
                            module: 'good-file',
                            args: ['./log.txt']
                        }],
                        myHTTPReporter: [{
                            module: 'good-squeeze',
                            name: 'Squeeze',
                            args: [{ error: '*' }]
                        }, {
                            module: 'good-http',
                            args: ['http://prod.logs:3000', {
                                wreck: {
                                    headers: { 'x-api-key': 12345 }
                                }
                            }]
                        }]
                    }
                }
            }
        },
        {
            plugin: {
                register: 'visionary',
                options: {
                    engines: { jade: 'jade' },
                    path: './server/web'
                }
            }
        },
        {
            plugin: {
                register: 'hapi-mongo-models',
                options: {
                    mongodb: Config.get('/hapiMongoModels/mongodb'),
                    models: {
                        Account: './server/models/account',
                        AdminGroup: './server/models/admin-group',
                        Admin: './server/models/admin',
                        AuthAttempt: './server/models/auth-attempt',
                        Session: './server/models/session',
                        Status: './server/models/status',

                        Pusher: './server/models/pusher',
                        IpLog: './server/models/ipLog',                        Pusher: './server/models/pusher',
                        Purchase: './server/models/purchase',
                        User: './server/models/user'
                    },
                    autoIndex: Config.get('/hapiMongoModels/autoIndex')
                }
            }
        },
        {
            plugin: './server/tasks'
        },
        {
            plugin: './server/auth'
        },
        {
            plugin: './server/mailer'
        },
        {
            plugin: './server/api/accounts',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/admin-groups',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/admins',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/auth-attempts',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/contact',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/index',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/login',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/logout',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/sessions',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/signup',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/statuses',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/api/users',
            options: {
                routes: { prefix: '/api' }
            }
        },
        {
            plugin: './server/web/index'
        }
    ]
};


const store = new Confidence.Store(manifest);


exports.get = function (key) {

    return store.get(key, criteria);
};


exports.meta = function (key) {

    return store.meta(key, criteria);
};
